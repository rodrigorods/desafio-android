package com.rodrigo.desafioandroid.bean;

import java.util.List;

public class GitCallResult {
    private List<Repository> items;

    public List<Repository> getItems() {
        return items;
    }
}
