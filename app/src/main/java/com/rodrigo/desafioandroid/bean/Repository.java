package com.rodrigo.desafioandroid.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Repository implements Parcelable{
    private String name;
    private String description;
    @SerializedName("forks")
    private int forksCount;
    @SerializedName("stargazers_count")
    private int starsCount;

    private Owner owner;

    Repository(){}

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getStarsCount() {
        return starsCount;
    }

    public int getForksCount() {
        return forksCount;
    }

    public Owner getOwner() {
        return owner;
    }

    //**
    // *PARCEL CONTENT
    // *//

    Repository(Parcel in){
        this.name = in.readString();
        this.description = in.readString();
        this.forksCount = in.readInt();
        this.starsCount = in.readInt();
        this.owner = in.readParcelable(Owner.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeInt(this.forksCount);
        dest.writeInt(this.starsCount);
        dest.writeParcelable(this.owner, flags);
    }

    public static final Parcelable.Creator<Repository> CREATOR = new Parcelable.Creator<Repository>() {
        @Override
        public Repository createFromParcel(Parcel in) {
            return new Repository(in);
        }
        @Override
        public Repository[] newArray(int size) {
            return new Repository[size];
        }
    };
}
