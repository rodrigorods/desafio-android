package com.rodrigo.desafioandroid.app;

import com.rodrigo.desafioandroid.BuildConfig;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class DesafioApiClient {

    private static final String API_ENDPOINT = "https://api.github.com/";

    public static <T> T createInstance(Class<T> instanceName) {
        Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl(API_ENDPOINT);
        builder.addCallAdapterFactory(RxJavaCallAdapterFactory.create());

        builder.addConverterFactory(GsonConverterFactory.create()); //TODO Testar assim

        if (BuildConfig.DEBUG) {
            builder.client(getLogLevelClient());
        }

        return builder.build().create(instanceName);
    }

    private static OkHttpClient getLogLevelClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().
                addInterceptor(interceptor).build();

        return client;
    }
}