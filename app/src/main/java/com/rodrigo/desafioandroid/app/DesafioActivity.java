package com.rodrigo.desafioandroid.app;

import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;

public class DesafioActivity extends AppCompatActivity{

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this); //Just to save one line on parent Activity
    }

}
