package com.rodrigo.desafioandroid.repositories;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.rodrigo.desafioandroid.R;
import com.rodrigo.desafioandroid.app.DesafioActivity;
import com.rodrigo.desafioandroid.app.RecyclerItemDecorator;
import com.rodrigo.desafioandroid.bean.Repository;
import com.rodrigo.desafioandroid.util.SystemUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class MainFeedActivity extends DesafioActivity implements IMainFeedView{

    private static final String STATE_REPOSITORIES = "012*ASkxa,hvs";
    private static final String STATE_CURRENT_PAGE = "0978GCAL@Udt7";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.repositories_list)
    RecyclerView repositoriesList;

    @BindView(R.id.error_holder)
    View errorHolder;
    @BindView(R.id.error_tv)
    TextView errorTv;
    @BindView(R.id.loading_pb)
    ProgressBar loading;

    private IMainFeedPresenter presenter;
    private LinearLayoutManager mLayoutManager;

    private boolean isLoadingNextPage = false;
    private int currentPage = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_feed);

        toolbar.setTitle(R.string.title_repositories);
        setSupportActionBar(toolbar);

        repositoriesList.setHasFixedSize(true);
        repositoriesList.addItemDecoration(new RecyclerItemDecorator(getBaseContext()));
        repositoriesList.addOnScrollListener(recycleScrollListener);

        mLayoutManager = (LinearLayoutManager) repositoriesList.getLayoutManager();

        presenter = new RepositoriesFeedPresenter(this);
        if (savedInstanceState == null) {
            loadRepositorios();
        } else {
            reloadActivityState(savedInstanceState);
        }
    }

    private void loadRepositorios() {
        repositoriesList.setVisibility(View.GONE);
        errorHolder.setVisibility(View.VISIBLE);
        loading.setVisibility(View.VISIBLE);
        errorTv.setVisibility(View.GONE);

        if (SystemUtils.isNetworkAvailable(getBaseContext())) {
            presenter.loadRepositories(currentPage);
        } else {
            handleRepositoryLoadError(RepositoryFeedErrorCause.NO_INTERNET_ERROR);
        }
    }

    private void reloadActivityState(Bundle savedInstanceState) {
        currentPage = savedInstanceState.getInt(STATE_CURRENT_PAGE);
        List<Repository> repositories = savedInstanceState.getParcelableArrayList(STATE_REPOSITORIES);
        showFeedList(repositories);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.reload_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_reload:
                loadRepositorios();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void showFeedList(List<Repository> repositories) {
        isLoadingNextPage = false;
        repositoriesList.setVisibility(View.VISIBLE);
        errorHolder.setVisibility(View.GONE);

        RepositoriesFeedAdapter adapter = (RepositoriesFeedAdapter) repositoriesList.getAdapter();
        if (adapter == null) {
            adapter = new RepositoriesFeedAdapter(repositories);
            repositoriesList.setAdapter(adapter);
        } else {
            adapter.insertNewItems(repositories);
        }
    }

    @Override
    public void handleRepositoryLoadError(RepositoryFeedErrorCause errorCause) {
        isLoadingNextPage = false;
        int errorMsgId = errorCause == RepositoryFeedErrorCause.NO_INTERNET_ERROR ?
                R.string.error_msg_no_internet : R.string.error_msg_unknown;

        boolean isFirstLoad = repositoriesList.getAdapter() == null;
        if (isFirstLoad) {
            repositoriesList.setVisibility(View.GONE);
            loading.setVisibility(View.GONE);
            errorHolder.setVisibility(View.VISIBLE);
            errorTv.setVisibility(View.VISIBLE);

            errorTv.setText(errorMsgId);
        } else {
            showSnackbakMsg(repositoriesList, errorMsgId);
        }
    }

    /**
     * Handling when user get to the end of RecycleView
     * */
    RecyclerView.OnScrollListener recycleScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if(dy > 0)
            {
                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                if (!isLoadingNextPage)
                {
                    if ((visibleItemCount + pastVisiblesItems) >= totalItemCount)
                    {
                        isLoadingNextPage = true;
                        currentPage++;
                        presenter.loadRepositories(currentPage);
                    }
                }
            }
        }
    };

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        RepositoriesFeedAdapter adapter = (RepositoriesFeedAdapter) repositoriesList.getAdapter();
        if (adapter != null) {
            ArrayList<Repository> repositories = (ArrayList<Repository>) adapter.getAll();
            outState.putParcelableArrayList(STATE_REPOSITORIES, repositories);
            outState.putInt(STATE_CURRENT_PAGE, currentPage);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.clean();
    }

    protected void showSnackbakMsg(View view, int msgId) {
        Snackbar.make(view, msgId, Snackbar.LENGTH_LONG).show();
    }
}
