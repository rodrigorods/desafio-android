package com.rodrigo.desafioandroid.repositories;

import com.rodrigo.desafioandroid.app.DesafioApiClient;
import com.rodrigo.desafioandroid.bean.GitCallResult;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

class RepositoriesFeedPresenter implements IMainFeedPresenter{

    private static final String GIT_KEYWORD_PARAM = "language:Java";
    private static final String GIT_SORT_TYPE_PARAM = "stars"; //stars, forks, or updated.

    private RepositoryFeedApi mRepositoryFeedApi;
    private IMainFeedView mFeedView;

    private Subscription mSubscription;

    RepositoriesFeedPresenter(IMainFeedView mainFeedView) {
        this.mFeedView = mainFeedView;
        this.mRepositoryFeedApi = DesafioApiClient.createInstance(RepositoryFeedApi.class);
    }

    @Override
    public void loadRepositories(int page) {
        mSubscription = mRepositoryFeedApi.getRepositories(GIT_KEYWORD_PARAM, GIT_SORT_TYPE_PARAM, page)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(myObserver);
    }

    private Observer<GitCallResult> myObserver = new Observer<GitCallResult>() {
        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            mFeedView.handleRepositoryLoadError(RepositoryFeedErrorCause.UNKNOWN_ERROR);
        }

        @Override
        public void onNext(GitCallResult result) {
            mFeedView.showFeedList(result.getItems());
        }
    };

    public void clean() {
        if (mSubscription != null && !mSubscription.isUnsubscribed())
            mSubscription.unsubscribe();
    }
}
