package com.rodrigo.desafioandroid.repositories;

import com.rodrigo.desafioandroid.bean.GitCallResult;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

interface RepositoryFeedApi {
    @GET("search/repositories")
    Observable<GitCallResult> getRepositories(@Query("q") String language, @Query("sort") String sortType, @Query("page") int page);
}
