package com.rodrigo.desafioandroid.repositories;

enum RepositoryFeedErrorCause {
    NO_INTERNET_ERROR,
    UNKNOWN_ERROR
}
