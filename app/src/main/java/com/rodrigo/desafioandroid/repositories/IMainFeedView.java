package com.rodrigo.desafioandroid.repositories;

import com.rodrigo.desafioandroid.bean.Repository;

import java.util.List;

interface IMainFeedView {
    void showFeedList(List<Repository> repopsitories);
    void handleRepositoryLoadError(RepositoryFeedErrorCause errorCause);
}
