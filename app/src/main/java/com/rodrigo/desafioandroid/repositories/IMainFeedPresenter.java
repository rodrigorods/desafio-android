package com.rodrigo.desafioandroid.repositories;

interface IMainFeedPresenter {
    void loadRepositories(int page);
    void clean();
}
