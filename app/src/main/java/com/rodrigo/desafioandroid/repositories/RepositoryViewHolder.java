package com.rodrigo.desafioandroid.repositories;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.rodrigo.desafioandroid.R;
import com.rodrigo.desafioandroid.bean.Repository;
import com.rodrigo.desafioandroid.pullrequests.PullRequestsActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

class RepositoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    @BindView(R.id.rp_name_tv)
    TextView nameTv;
    @BindView(R.id.rp_description_tv)
    TextView descriptionTv;
    @BindView(R.id.rp_fork_count_tv)
    TextView forkCountTv;
    @BindView(R.id.rp_star_count_tv)
    TextView starCountTv;
    @BindView(R.id.rp_owner_profile)
    ImageView ownerImg;
    @BindView(R.id.rp_owner_name)
    TextView ownerNameTv;

    RepositoryViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

        itemView.setOnClickListener(this);

        forkCountTv.setCompoundDrawablesWithIntrinsicBounds(
                tintDrawable(itemView.getResources(), R.drawable.ic_call_split_black_24dp, R.color.colorCellGolden)
                , null, null, null);

        starCountTv.setCompoundDrawablesWithIntrinsicBounds(
                tintDrawable(itemView.getResources(), R.drawable.ic_grade_black_24dp, R.color.colorCellGolden)
                , null, null, null);
    }

    void fillContent(Repository repository) {
        itemView.setTag(repository);

        nameTv.setText(repository.getName());
        descriptionTv.setText(repository.getDescription());
        forkCountTv.setText(Integer.toString(repository.getForksCount()));
        starCountTv.setText(Integer.toString(repository.getStarsCount()));
        ownerNameTv.setText(repository.getOwner().getLogin());

        Glide.with(itemView.getContext())
                .load(repository.getOwner().getAvatarUrl())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .fitCenter()
                .into(ownerImg);
    }

    private Drawable tintDrawable (Resources resources, int drawableId, int colorId) {
        Drawable drawable = resources.getDrawable(drawableId);
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable.mutate(), resources.getColor(colorId));
        return drawable;
    }

    @Override
    public void onClick(View v) {
        Repository rp = (Repository) v.getTag();

        Context context = v.getContext();
        Intent it = new Intent(context, PullRequestsActivity.class);
        it.putExtra(PullRequestsActivity.REPOSITORY_OWNER_PULL_EXTRA, rp.getOwner().getLogin());
        it.putExtra(PullRequestsActivity.REPOSITORY_NAME_PULL_EXTRA, rp.getName());

        context.startActivity(it);
    }
}
