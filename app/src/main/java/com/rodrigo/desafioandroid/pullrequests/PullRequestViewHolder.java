package com.rodrigo.desafioandroid.pullrequests;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.rodrigo.desafioandroid.R;
import com.rodrigo.desafioandroid.bean.PullRequest;

import butterknife.BindView;
import butterknife.ButterKnife;

class PullRequestViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    @BindView(R.id.pr_title_tv)
    TextView nameTv;
    @BindView(R.id.pr_description_tv)
    TextView descriptionTv;
    @BindView(R.id.pr_owner_profile)
    ImageView ownerImg;
    @BindView(R.id.pr_owner_name)
    TextView ownerNameTv;

    PullRequestViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        itemView.setOnClickListener(this);
    }

    void fillContent(PullRequest pr) {
        itemView.setTag(pr);
        nameTv.setText(pr.getTitle());
        descriptionTv.setText(pr.getDescription());
        ownerNameTv.setText(pr.getOwner().getLogin());

        Glide.with(itemView.getContext())
                .load(pr.getOwner().getAvatarUrl())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .fitCenter()
                .into(ownerImg);
    }

    @Override
    public void onClick(View v) {
        PullRequest pr = (PullRequest) v.getTag();

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(pr.getContentUrl()));
        v.getContext().startActivity(browserIntent);
    }

}
