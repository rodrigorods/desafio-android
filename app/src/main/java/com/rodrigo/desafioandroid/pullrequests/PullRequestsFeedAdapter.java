package com.rodrigo.desafioandroid.pullrequests;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rodrigo.desafioandroid.R;
import com.rodrigo.desafioandroid.bean.PullRequest;

import java.util.List;

class PullRequestsFeedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

   private List<PullRequest> pullRequests;

    PullRequestsFeedAdapter(List<PullRequest> repositories) {
        pullRequests = repositories;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(parent.getContext());
        View cellView = mInflater.inflate(R.layout.cell_pull_request, parent, false);
        return new PullRequestViewHolder(cellView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((PullRequestViewHolder)holder).fillContent(pullRequests.get(position));
    }

    @Override
    public int getItemCount() {
        return pullRequests.size();
    }

    List<PullRequest> getPullRequests() {
        return pullRequests;
    }
}
