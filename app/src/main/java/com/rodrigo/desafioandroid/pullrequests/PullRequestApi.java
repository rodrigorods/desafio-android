package com.rodrigo.desafioandroid.pullrequests;

import com.rodrigo.desafioandroid.bean.PullRequest;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

interface PullRequestApi {
    @GET("repos/{owner_name}/{repository_name}/pulls")
    Observable<List<PullRequest>> getPullRequests(@Path("owner_name") String ownerName, @Path("repository_name") String repositoryName);
}
