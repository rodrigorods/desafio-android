package com.rodrigo.desafioandroid.pullrequests;

enum PullRequestErrorCause {
    NO_INTERNET_ERROR,
    UNKNOWN_ERROR
}
