package com.rodrigo.desafioandroid.pullrequests;

import com.rodrigo.desafioandroid.app.DesafioApiClient;
import com.rodrigo.desafioandroid.bean.PullRequest;

import java.util.List;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

class PullRequestPresenter implements IPullRequestPresenter{

    private PullRequestApi mPullRequestApi;
    private IPullRequestView mView;

    private Subscription mSubscription;

    private String mRepoName, mOwnerName;

    PullRequestPresenter(IPullRequestView view, String repoName, String ownerName){
        this.mView = view;
        this.mRepoName = repoName;
        this.mOwnerName = ownerName;

        mPullRequestApi = DesafioApiClient.createInstance(PullRequestApi.class);
    }

    @Override
    public void loadPullRequests() {
        //TODO https://medium.com/@kurtisnusbaum/rxandroid-basics-part-2-6e877af352#.4tofvscw0
        mSubscription = mPullRequestApi.getPullRequests(this.mOwnerName, this.mRepoName)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(myObserver);
    }

    private Observer<List<PullRequest>> myObserver = new Observer<List<PullRequest>>() {
        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            mView.handleLoadError(PullRequestErrorCause.UNKNOWN_ERROR);
        }

        @Override
        public void onNext(List<PullRequest> result) {
            mView.showPullRequests(result);
        }
    };

    public void clean() {
        if (mSubscription != null && !mSubscription.isUnsubscribed())
            mSubscription.unsubscribe();
    }
}
