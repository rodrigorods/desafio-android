package com.rodrigo.desafioandroid.pullrequests;

interface IPullRequestPresenter {
    void loadPullRequests();
    void clean();
}
