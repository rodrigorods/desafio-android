package com.rodrigo.desafioandroid.pullrequests;

import com.rodrigo.desafioandroid.bean.PullRequest;

import java.util.List;

interface IPullRequestView {
    void showPullRequests(List<PullRequest> pullrequestes);
    void handleLoadError(PullRequestErrorCause errorCause);
}
