package com.rodrigo.desafioandroid.pullrequests;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.rodrigo.desafioandroid.R;
import com.rodrigo.desafioandroid.app.DesafioActivity;
import com.rodrigo.desafioandroid.app.RecyclerItemDecorator;
import com.rodrigo.desafioandroid.bean.PullRequest;
import com.rodrigo.desafioandroid.util.SystemUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class PullRequestsActivity extends DesafioActivity implements IPullRequestView{

    private static final String STATE_PR_FEED = "AS)C(&*H@LasAs";
    public static final String REPOSITORY_NAME_PULL_EXTRA = "AP9I783DGHD3P";
    public static final String REPOSITORY_OWNER_PULL_EXTRA = "(*A&CXGLA@IUd";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.pr_list)
    RecyclerView repositoriesList;

    private IPullRequestPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request_list);

        String repoName = getIntent().getStringExtra(REPOSITORY_NAME_PULL_EXTRA);
        String ownerName = getIntent().getStringExtra(REPOSITORY_OWNER_PULL_EXTRA);

        toolbar.setTitle(getString(R.string.title_pull_request, ownerName, repoName));
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        setSupportActionBar(toolbar);

        presenter = new PullRequestPresenter(this, repoName, ownerName);
        if (savedInstanceState == null) {
            if (SystemUtils.isNetworkAvailable(getBaseContext())) {
                presenter.loadPullRequests();
            } else {
                handleLoadError(PullRequestErrorCause.NO_INTERNET_ERROR);
            }
        } else {
            reloadActivityState(savedInstanceState);
        }

        repositoriesList.setHasFixedSize(false);
        repositoriesList.addItemDecoration(new RecyclerItemDecorator(getBaseContext()));
    }

    private void reloadActivityState(Bundle savedInstanceState) {
        List<PullRequest> pullRequestList = savedInstanceState.getParcelableArrayList(STATE_PR_FEED);
        showPullRequests(pullRequestList);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        PullRequestsFeedAdapter adapter = (PullRequestsFeedAdapter) repositoriesList.getAdapter();
        if (adapter != null) {
            ArrayList<PullRequest> pullRequests = (ArrayList<PullRequest>) adapter.getPullRequests();
            outState.putParcelableArrayList(STATE_PR_FEED, pullRequests);
        }
    }

    @Override
    public void showPullRequests(List<PullRequest> pullrequestes) {
        repositoriesList.setAdapter(new PullRequestsFeedAdapter(pullrequestes));
    }

    @Override
    public void handleLoadError(PullRequestErrorCause errorCause) {
        int errorMsgId = errorCause == PullRequestErrorCause.NO_INTERNET_ERROR ?
                R.string.error_msg_no_internet : R.string.error_msg_unknown;

        Snackbar.make(repositoriesList, errorMsgId, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.try_again, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        presenter.loadPullRequests();
                    }
                })
                .show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.clean();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.menu_reload:
                //TODO Recarregar
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
